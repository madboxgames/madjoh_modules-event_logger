define([
	'require',
	'madjoh_modules/facebook/facebook',
	'madjoh_modules/custom_events/custom_events'
],
function (require, FacebookAPI, CustomEvents){
	var EventLogger = {
		init : function(){
			amplitude.init('71b7004ba0842f681c8de46644333403');
			CustomEvents.addCustomEventListener(document, 'LogEvent', function(doc, data){EventLogger.log(data.event, data.type, data.add);});
		},

		log : function(event, type, add){
			if(document.location.hostname === 'localhost') return;

			if(!add) add = 1;

			var data = type ? {type : type} : null;

			amplitude.getInstance().logEvent(event, data);
			EventLogger.logFacebook(event, data, add);
			EventLogger.logFabric(event, data);
		},
		logFacebook : function(name, params, valueToSum){
			return new Promise(function(resolve, reject){
				FacebookAPI.init().then(function(){
					if(window.cordova){
						if(!params) params = {};
						var onSuccess = function(result){
							console.log('Logged FB Event ' + name);
							resolve();
						};
						var onFailure = function(error){
							console.log('Failed Logging FB Event ' + name);
							console.log(error);
							reject(error);
						};
						facebookConnectPlugin.logEvent(name, params, valueToSum, onSuccess, onFailure);
					}
				});
			});
		},
		logFabric : function(name, params){
			if(!window.fabric || !window.fabric.Answers) return;

			if(name === 'ChangePage') 				return window.fabric.Answers.sendScreenView(params.type, params.type);
			if(name === 'Complete Registration') 	return window.fabric.Answers.sendSignUp(params.type);
			if(name === 'LoggedIn') 				return window.fabric.Answers.sendLogIn(params.type);

			window.fabric.Answers.sendCustomEvent(name, params);
		}
	};

	return EventLogger;
});
